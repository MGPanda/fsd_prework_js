function compararRuedas(diametro) {
  if (diametro <= 10) {
    console.log("Es una rueda para un juguete pequeño.");
  } else if (diametro > 10 && diametro < 20) {
    console.log("Es una rueda para un juguete mediano.");
  } else if (diametro >= 20) {
    console.log("Es una rueda para un juguete grande.");
  }
}

compararRuedas(5);
compararRuedas(15);
compararRuedas(25);
