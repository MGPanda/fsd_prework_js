var letras = [
  "T",
  "R",
  "W",
  "A",
  "G",
  "M",
  "Y",
  "F",
  "P",
  "D",
  "X",
  "B",
  "N",
  "J",
  "Z",
  "S",
  "Q",
  "V",
  "H",
  "L",
  "C",
  "K",
  "E",
  "T",
];

function comprobarDni() {
  var numeroDni = prompt("Introduce el número de tu DNI");
  var letraDni = prompt("Ahora, introduce la letra").toUpperCase;

  if (numeroDni > 0 && numeroDni < 99999999) {
    var residuo = numeroDni % 23;
    if (letraDni == letras[residuo]) {
      console.log("DNI válido.");
    } else {
      console.log("DNI inválido.");
    }
  } else {
    console.log("Número inválido.");
  }
}
